import re
import pandas
from bs4 import BeautifulSoup


def getID(name, nameValue):
    df = pandas.read_csv('details/' + name + '.csv')
    for j in range(len(df[name])):
        if nameValue == df[name][j]:
            return df['ID'][j]


acted_in_data = pandas.DataFrame()
directed_data = pandas.DataFrame()
write_data = pandas.DataFrame()
belong_to_data = pandas.DataFrame()
act_dir_cooperation_data = pandas.DataFrame()
act_auth_cooperation_data = pandas.DataFrame()
dir_auth_cooperation_data = pandas.DataFrame()


def save_relation(start_id, end_id, relation):
    dataframe = pandas.DataFrame({':START_ID': start_id, ':END_ID': end_id, ':relation': relation, ':TYPE': relation})
    dataframe.to_csv('details/' + relation + '.csv', index=False, sep=',', encoding="utf_8_sig")


# 电影-演员
def save_acted_in(content):
    # 获取当前电影对应ID
    film_name = re.findall('<title>.*?/title>', content)[0]
    film_name = film_name.lstrip("<title>").rstrip("(豆瓣)</title>").replace(" ", "")  # 电影名字每页只有一个
    filmNameID = getID('film_name', film_name)

    # 获取当前电影的演员和对应ID
    actor_cont = re.findall('"actor":.*?]', content)[0]
    actor_cont = re.findall('"name": ".*?"', actor_cont)
    for i in range(len(actor_cont)):  # 演员每页可能多个（通常都多个)
        actor = actor_cont[i].lstrip('name": "').rstrip('"')
        start_id.append(getID('actor', actor))
        end_id.append(filmNameID)  # 查找演员名字对应ID


# 电影-导演
def save_directed(contnet):
    # 获取当前电影对应ID
    film_name = re.findall('<title>.*?/title>', content)[0]
    film_name = film_name.lstrip("<title>").rstrip("(豆瓣)</title>").replace(" ", "")
    filmNameID = getID('film_name', film_name)

    # 获取当前电影的导演和对应ID
    director_cont = re.findall('"director":.*?]', content)[0]
    director_cont = re.findall('"name": ".*?"', director_cont)
    for i in range(len(director_cont)):
        director = director_cont[i].lstrip('"name": "').rstrip('"')
        start_id.append(getID('director', director))
        end_id.append(filmNameID)


# 电影-编剧
def save_write(content):
    # 获取当前电影对应ID
    film_name = re.findall('<title>.*?/title>', content)[0]
    film_name = film_name.lstrip("<title>").rstrip("(豆瓣)</title>").replace(" ", "")
    filmNameID = getID('film_name', film_name)

    # 获取当前电影的编剧和对应ID
    author_cont = re.findall('"author":.*?]', content)[0]
    author_cont = re.findall('"name": ".*?"', author_cont)
    for i in range(len(author_cont)):
        author = author_cont[i].lstrip('"name": "').rstrip('"')
        start_id.append(getID('author', author))
        end_id.append(filmNameID)


# 电影-类型
def save_belongto(content):
    # 获取当前电影对应ID
    film_name = re.findall('<title>.*?/title>', content)[0]
    film_name = film_name.lstrip("<title>").rstrip("(豆瓣)</title>").replace(" ", "")
    filmNameID = getID('film_name', film_name)

    # 获取当前电影的类型和对应ID
    type_cont = re.findall('<span property="v:genre">.*?</span>', content)
    for i in range(len(type_cont)):
        type = type_cont[i].lstrip('<span property="v:genre">').rstrip('</span>')
        start_id.append(filmNameID)
        end_id.append(getID('type', type))


# 演员-导演
def save_act_dir_cooperation(content):
    # 获取当前电影的演员和对应ID
    actor_cont = re.findall('"actor":.*?]', content)[0]
    actor_cont = re.findall('"name": ".*?"', actor_cont)

    # 获取当前电影的导演和对应ID
    director_cont = re.findall('"director":.*?]', content)[0]
    director_cont = re.findall('"name": ".*?"', director_cont)

    for i in range(len(actor_cont)):
        actor = actor_cont[i].lstrip('name": "').rstrip('"')
        for j in range(len(director_cont)):
            director = director_cont[j].lstrip('"name": "').rstrip('"')
            start_id.append(getID('actor', actor))
            end_id.append(getID('director', director))


# 演员-编剧
def save_act_auth_cooperation(content):
    # 获取当前电影的演员和对应ID
    actor_cont = re.findall('"actor":.*?]', content)[0]
    actor_cont = re.findall('"name": ".*?"', actor_cont)

    # 获取当前电影的编剧和对应ID
    author_cont = re.findall('"author":.*?]', content)[0]
    author_cont = re.findall('"name": ".*?"', author_cont)

    for i in range(len(actor_cont)):
        actor = actor_cont[i].lstrip('name": "').rstrip('"')
        for j in range(len(author_cont)):
            author = author_cont[j].lstrip('"name": "').rstrip('"')
            start_id.append(getID('actor', actor))
            end_id.append(getID('author', author))


# 导演-编剧
def save_dir_auth_cooperation(content):
    # 获取当前电影的导演和对应ID
    director_cont = re.findall('"director":.*?]', content)[0]
    director_cont = re.findall('"name": ".*?"', director_cont)

    # 获取当前电影的编剧和对应ID
    author_cont = re.findall('"author":.*?]', content)[0]
    author_cont = re.findall('"name": ".*?"', author_cont)

    for i in range(len(director_cont)):
        director = director_cont[i].lstrip('name": "').rstrip('"')
        for j in range(len(author_cont)):
            author = author_cont[j].lstrip('"name": "').rstrip('"')
            start_id.append(getID('director', director))
            end_id.append(getID('author', author))


# 电影-短评
def save_short_comment(content):
    # 获取当前电影对应ID
    film_name = re.findall('<title>.*?/title>', content)[0]
    film_name = film_name.lstrip("<title>").rstrip("(豆瓣)</title>").replace(" ", "")  # 电影名字每页只有一个
    filmNameID = getID('film_name', film_name)

    # 获取当前电影的短评内容和对应ID
    soup = BeautifulSoup(content, 'html.parser')
    comment_divs = soup.select('#hot-comments > div > div')
    for div in comment_divs:
        sc_content_cont = div.select('p')[0].get_text().strip()
        start_id.append(filmNameID)
        end_id.append(getID('sc_content', sc_content_cont))  # 查找短评内容对应ID


# 电影-影评
def save_comment(content):
    # 获取当前电影对应ID
    film_name = re.findall('<title>.*?/title>', content)[0]
    film_name = film_name.lstrip("<title>").rstrip("(豆瓣)</title>").replace(" ", "")  # 电影名字每页只有一个
    filmNameID = getID('film_name', film_name)

    # 获取当前电影的短评内容和对应ID
    soup = BeautifulSoup(content, 'html.parser')
    comment_divs = soup.select('#reviews-wrapper > div.review-list > div')
    for div in comment_divs:
        title_cont = div.select('div.main.review-item > div.main-bd > h2 > a')[0].get_text().strip()
        start_id.append(filmNameID)
        end_id.append(getID('c_title', title_cont))  # 查找短评内容对应ID


# 用来存放关系节点ID的列表
start_id = []
end_id = []

# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(acted_in)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_acted_in(content)
save_relation(start_id, end_id, 'acted_in')
print('save acted_in finished!')

start_id.clear()
end_id.clear()
# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(directed)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_directed(content)
save_relation(start_id, end_id, 'directed')
print('save directed finished!')

start_id.clear()
end_id.clear()
# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(write)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_write(content)
save_relation(start_id, end_id, 'write')
print('save write finished!')

start_id.clear()
end_id.clear()
# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(belong_to)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_belongto(content)
save_relation(start_id, end_id, 'belong_to')
print('save belong_to finished!')

start_id.clear()
end_id.clear()
# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(act_dir_cooperation)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_act_dir_cooperation(content)
save_relation(start_id, end_id, 'act_dir_cooperation')
print('save act_dir_cooperation finished!')

start_id.clear()
end_id.clear()
# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(act_auth_cooperation)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_act_auth_cooperation(content)
save_relation(start_id, end_id, 'act_auth_cooperation')
print('save act_auth_cooperation finished!')

start_id.clear()
end_id.clear()
# # 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(dir_auth_cooperation)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_dir_auth_cooperation(content)
save_relation(start_id, end_id, 'dir_auth_cooperation')
print('save dir_auth_cooperation finished!')

start_id.clear()
end_id.clear()
# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(short_comment)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_short_comment(content)
save_relation(start_id, end_id, 'short_comment')
print('save short_comment finished!')

start_id.clear()
end_id.clear()
# 循环查找每个页面（即contents文件夹中下载下来的页面），找出对应关系(comment)
for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        content = f.read().replace('\n', "")  # 要去掉换行符
    save_comment(content)
save_relation(start_id, end_id, 'comment')
print('save comment finished!')
