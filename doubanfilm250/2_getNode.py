import re
import pandas

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
}


def node_save(attrCont, tag, attr, label):
    ID = []
    for i in range(len(attrCont)):
        ID.append(tag * 10000 + i)
    data = {'ID': ID, attr: attrCont, 'LABEL': label}
    dataframe = pandas.DataFrame(data)
    dataframe.to_csv('details/' + attr + '.csv', index=False, sep=',', encoding="utf_8_sig")


def save(contents):
    # save movie nodes
    country_cont = re.findall('<span class="pl">制片国家/地区:</span> (.*?)<br/>', contents)
    language_cont = re.findall('<span class="pl">语言:</span> (.*?)<br/>', contents)
    release_date_cont = re.findall(r'上映日期:</span> <span property="v:initialReleaseDate".*?>(.*?)</span>', contents)
    duration_cont = re.findall(r'片长:</span> <span property="v:runtime".*?>(.*?)</span>', contents)
    alias_cont = re.findall(r'又名:</span> (.*?)<br/>', contents)
    rating_cont = re.findall('<strong class="ll rating_num" property="v:average">(.*?)</strong>', contents)
    summary_cont = re.findall('<span property="v:summary".*?>(.*?)</span>', contents)

    film_name = re.findall('<title>.*?/title>', contents)[0]
    film_name = film_name.lstrip("<title>").rstrip("(豆瓣)</title>").replace(" ", "")
    film_names.append(film_name)

    # 将所有信息保存在对应的列表中，与电影名一一对应
    language.append(language_cont[0] if language_cont else '')
    release_date.append(release_date_cont[0] if release_date_cont else '')
    rating.append(rating_cont[0] if rating_cont else '')
    country.append(country_cont[0] if country_cont else '')
    duration.append(duration_cont[0] if duration_cont else '')
    summary.append(summary_cont[0] if summary_cont else '')
    alias.append(alias_cont[0] if alias_cont else '')

    # save director nodes
    director_cont = re.findall('"director":.*?]', contents)[0]
    director_cont = re.findall('"name": ".*?"', director_cont)
    for i in range(len(director_cont)):
        directors.append(director_cont[i].lstrip('"name": "').rstrip('"'))

    # save author nodes
    author_cont = re.findall('"author":.*?]', contents)[0]
    author_cont = re.findall('"name": ".*?"', author_cont)
    for i in range(len(author_cont)):
        authors.append(author_cont[i].lstrip('"name": "').rstrip('"'))

    # save actors nodes
    actor_cont = re.findall('"actor":.*?]', contents)[0]
    actor_cont = re.findall('"name": ".*?"', actor_cont)
    for i in range(len(actor_cont)):
        actors.append(actor_cont[i].lstrip('"name": "').rstrip('"'))

    # save type
    type_cont = re.findall('<span property="v:genre">.*?</span>', contents)
    for i in range(len(type_cont)):
        types.append(type_cont[i].lstrip('<span property="v:genre">').rstrip('</span>'))


language = []
release_date = []
rating = []
country = []
duration = []
summary = []
alias = []

film_names = []
actors = []
authors = []
directors = []
types = []

for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        contents = f.read()
    save(contents.replace("\n", ""))  # 这里需要把读出来的数据换行符去掉

# 去重
actors = list(set(actors))
directors = list(set(directors))
authors = list(set(authors))
types = list(set(types))
language = list(set(language))
release_date = list(set(release_date))
rating = list(set(rating))
country = list(set(country))
duration = list(set(duration))

# 保存
node_save(film_names, 0, 'film_name', 'movie')
node_save(language, 0, 'language', 'language')
node_save(release_date, 0, 'release_date', 'release_date')
node_save(rating, 0, 'rating', 'rating')
node_save(country, 0, 'country', 'country')
node_save(duration, 0, 'duration', 'duration')
node_save(summary, 0, 'summary', 'summary')
node_save(alias, 0, 'alias', 'alias')
node_save(directors, 1, 'director', 'person')
node_save(authors, 2, 'author', 'person')
node_save(actors, 3, 'actor', 'person')
node_save(types, 4, "type", "type")
print('ok1')
