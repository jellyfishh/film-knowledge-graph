from bs4 import BeautifulSoup
import pandas

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
}

sc_names = []
sc_times = []
sc_contents = []
c_names = []
c_times = []
c_titles = []
c_contents = []


def node_save(attrCont, tag, attr, label):
    ID = []
    for i in range(len(attrCont)):
        ID.append(tag * 10000 + i)
    data = {'ID': ID, attr: attrCont, 'LABEL': label}
    dataframe = pandas.DataFrame(data)
    dataframe.to_csv('details/' + attr + '.csv', index=False, sep=',', encoding="utf_8_sig")


def save(contents):
    # save short-comment node
    soup = BeautifulSoup(contents, 'html.parser')
    short_comment_divs = soup.select('#hot-comments > div > div')

    for div in short_comment_divs:
        try:
            sc_name_cont = div.select('h3 > span.comment-info > a')[0].get_text()
        except:
            sc_name_cont = ''
        try:
            sc_time_cont = div.select('h3 > span.comment-info > span.comment-time')[0].get('title')
        except:
            sc_time_cont = ''
        try:
            sc_content_cont = div.select('p')[0].get_text().strip()
        except:
            sc_content_cont = ''
        sc_names.append(sc_name_cont)
        sc_times.append(sc_time_cont)
        sc_contents.append(sc_content_cont)

    # save comment node
    soup = BeautifulSoup(contents, 'html.parser')
    comment_divs = soup.select('#reviews-wrapper > div.review-list > div')

    for div in comment_divs:
        try:
            name_cont = div.select('div.main.review-item > header > a.name')[0].get_text()
        except:
            name_cont = ''
        try:
            time_cont = div.select('div.main.review-item > header > span.main-meta')[0].get_text()
        except:
            time_cont = ''
        try:
            title_cont = div.select('div.main.review-item > div.main-bd > h2 > a')[0].get_text().strip()
        except:
            title_cont = ''
        try:
            content_cont = div.select('div.main.review-item > div.main-bd > div.review-short > div.short-content')[0].get_text().strip()
        except:
            content_cont = ''
        c_names.append(name_cont)
        c_times.append(time_cont)
        c_titles.append(title_cont)
        c_contents.append(content_cont)


for i in range(250):
    with open('contents/' + str(i) + '.txt', mode='r', encoding='utf8') as f:
        contents = f.read()
    save(contents.replace("\n", ""))

node_save(sc_names, 5, "sc_name", "person")
node_save(sc_times, 5, "sc_time", "time")
node_save(sc_contents, 5, "sc_content", "content")
node_save(c_names, 6, "c_name", "person")
node_save(c_times, 6, "c_time", "time")
node_save(c_titles, 6, "c_title", "title")
node_save(c_contents, 6, "c_content", "content")


print('ok2')
