# -*- coding: UTF-8 -*-
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from graph1.preprocess_data import Question
import json
# Create your views here.

que = Question()


def sidebar(request):
    return render(request, 'sidebar.html')


# 主页显示类
def handle_question(question):
    answer = que.question_process(question)
    if not answer:
        answer = "抱歉 暂时未查找到答案！"
    return answer


def handle_question2(question):
    result = que.question_process2(question)
    if not result:
        result = "抱歉 暂时未查找到答案！"
    return result


@csrf_exempt
def ajax_handle_question(request):
    if request.method == 'POST':
        question = request.POST.get('question', '')
        answer = handle_question(question)
        return JsonResponse({'answer': answer})
    return JsonResponse({'answer': ''})


relationCountDict = {}


# def sortDict(relationDict):
#     for i in range(len(relationDict)):
#         relationName = relationDict[i]['r']['relation']
#         relationCount = relationCountDict.get(relationName)
#         if relationCount is None:
#             relationCount = 0
#         relationDict[i]['relationCount'] = relationCount
#
#     relationDict = sorted(relationDict, key=lambda item: item['relationCount'], reverse=True)
#
#     return relationDict


def ajax_handle_question2(request):
    if request.method == 'POST':
        question = request.POST.get('question', '')
        result = handle_question2(question)
        # result = sortDict(result)
        return JsonResponse({'searchResult': json.dumps(result, ensure_ascii=False)})
    return JsonResponse({'searchResult': ''})


def add(request):
    if request.method == 'POST':
        question = request.POST.get('question', '')
        answer = handle_question(question)
        return JsonResponse({'answer': answer})
    return JsonResponse({'answer': ''})


# def add(request):
#     if request.method == 'POST':
#         question = request.POST.get('question', '')
#         answer = handle_question(question)
#         return render(request, 'QA.html', {'answer': answer})
#     return render(request, 'QA.html')


def QA(request):
    return render(request, 'QA.html')

def show_overview(request):
    return render(request, 'helicopter_view.html')