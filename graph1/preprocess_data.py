#-*- coding: UTF-8 -*-
'''
接收原始问题
对原始问题进行分词、词性标注等处理
对问题进行抽象
'''
from graph1.data import jieba
import re
from graph1.question_classification import Question_classify
from graph1.question_template import QuestionTemplate
from graph1.question_graph import QuestionGraph

import sys
import os

# Disable
# blockPrint()
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
# enablePrint()
def enablePrint():
    sys.stdout = sys.__stdout__

class Question():
    def __init__(self):
        # 初始化相关设置：读取词汇表，训练分类器，连接数据库
        self.init_config()
        self.question_word = []
        self.question_flag = []

    def init_config(self):
        # 训练分类器
        self.classify_model = Question_classify()
        # 读取问题模板
        with(open("graph1/data/question/question_classification.txt", "r", encoding="utf-8")) as f:
            question_mode_list = f.readlines()
        self.question_mode_dict = {}
        for one_mode in question_mode_list:
            # 读取一行
            mode_id, mode_str = str(one_mode).strip().split(":")
            # 处理一行，并存入
            self.question_mode_dict[int(mode_id)] = str(mode_str).strip()
        print(self.question_mode_dict)

        # 创建问题模板对象
        self.questiontemplate = QuestionTemplate()
        self.questiongraph = QuestionGraph()

    def question_process(self, question):
        # 接收问题
        self.raw_question = str(question).strip()
        # 对问题进行词性标注
        # self.pos_question = self.question_posseg()
        # print(self.pos_question)
        pos_temp, self.question_word, self.question_flag = self.question_posseg()
        # return self.pos_quesiton
        # 得到问题的模板
        # self.question_word, self.question_flag = self.question_posseg()
        self.question_template_id_str = self.get_question_template()
        print(self.question_template_id_str)

        pos_question, pos_word, pos_flag = self.question_posseg()
        # 查询图数据库,得到答案
        self.answer = self.query_template(pos_question)
        return(self.answer)

    def question_posseg(self):
        jieba.load_userdict("graph1/data/userdict2.txt")

        with open("graph1/data/userdict1.txt", "r", encoding="utf-8") as f:
            for line in f:
                line = line.strip()
                if line:
                   word, freq, pos = line.split("@@")
                   # jieba.re_han_default = re.compile("([\u4E00-\u9FD5a-zA-Z0-9+#&\._% ]+)", re.U)
                   jieba.add_word(word, freq=int(freq), tag=pos)
        # jieba.add_word("音乐", freq=883635, tag="tp")
        # jieba.add_word("冒险", freq=883635, tag="tp")
        # jieba.add_word("剧情", freq=883635, tag="tp")
        # jieba.add_word("歌舞", freq=883635, tag="tp")
        # jieba.add_word("武侠", freq=883635, tag="tp")
        # jieba.add_word("惊悚", freq=883635, tag="tp")
        # jieba.add_word("西部", freq=883635, tag="tp")
        # jieba.add_word("传记", freq=883635, tag="tp")
        # jieba.add_word("纪录片", freq=883635, tag="tp")
        # jieba.add_word("悬疑", freq=883635, tag="tp")
        # jieba.add_word("恐怖", freq=883635, tag="tp")
        # jieba.add_word("历史", freq=883635, tag="tp")
        # jieba.add_word("同性", freq=883635, tag="tp")
        # jieba.add_word("运动", freq=883635, tag="tp")
        # jieba.add_word("奇幻", freq=883635, tag="tp")
        # jieba.add_word("儿童", freq=883635, tag="tp")
        # jieba.add_word("动作", freq=883635, tag="tp")
        # jieba.add_word("灾难", freq=883635, tag="tp")
        # jieba.add_word("科幻", freq=883635, tag="tp")
        # jieba.add_word("爱情", freq=883635, tag="tp")
        # jieba.add_word("情色", freq=883635, tag="tp")
        # jieba.add_word("家庭", freq=883635, tag="tp")
        # jieba.add_word("犯罪", freq=883635, tag="tp")
        # jieba.add_word("喜剧", freq=883635, tag="tp")
        # jieba.add_word("战争", freq=883635, tag="tp")
        # jieba.add_word("动画", freq=883635, tag="tp")
        # jieba.add_word("古装", freq=883635, tag="tp")
        clean_question = self.raw_question
        # clean_question = re.sub("[\s+\.\!\/_,$%^*(+\"\')]+|[+——()?【】“”！，。？、~@#￥%……&*（）]+","", self.raw_question)
        self.clean_question = clean_question
        jieba.re_han_default = re.compile("([\u4E00-\u9FD5a-zA-Z0-9+#&\._% ]+)", re.U)
        question_sep = jieba.cut(str(clean_question))
        question_sel = list(question_sep)
        result = question_sel
        question_flag = []
        # for i in range(len(question_sel)):
        #     with open("./data/userdict1.txt", "r", encoding="utf-8") as f:
        #         for line in f:
        #             line = line.strip()
        #             if line:
        #                 word, freq, pos = line.split("@@")
        #                 if question_sel[i] == word:
        #                     self.question_flag[i] = pos
        #                     result[i] = f'{question_sel[i]}/{pos}'
                            # print(question_sel[i])
        for i in range(len(question_sel)):
            with open("graph1/data/userdict1.txt", "r", encoding="utf-8") as f:
                word_found = False
                for line in f:
                    line = line.strip()
                    if line:
                        word, freq, pos = line.split("@@")
                        if question_sel[i] == word:
                            # self.question_flag.append(pos)
                            question_flag.append(pos)
                            result[i] = f'{question_sel[i]}/{pos}'
                            # self.question_word = question_sel
                            word_found = True
                            break
                if not word_found:
                    # 如果没有在词典中找到对应词性，则使用默认的词性标记
                    # self.question_flag.append("undefined")
                    question_flag.append("undefined")
                    result[i] = f'{question_sel[i]}/undefined'
        self.question_word = question_sel
        print(result)
        return result, question_sel, question_flag
        # question_seged = jieba.posseg.cut(str(clean_question))
        # result = []
        # question_word, question_flag = [], []
        # for w in question_seged:
        #     temp_word = f"{w.word}/{w.flag}"
        #     result.append(temp_word)
        #     # 预处理问题
        #     word, flag = w.word, w.flag
        #     question_word.append(str(word).strip())
        #     question_flag.append(str(flag).strip())
        # assert len(question_flag) == len(question_word)
        # self.question_word = question_word
        # self.question_flag = question_flag
        # print(question_flag)
        # return result

    def get_question_template(self):
        # 抽象问题
        for item in ['nm', 'tp', 'ng', 'act']:
            while (item in self.question_flag):
                ix = self.question_flag.index(item)
                self.question_word[ix] = item  #词替换为词性
                self.question_flag[ix] = item + "ed"
        # 将问题转化字符串
        str_question = "".join(self.question_word)
        print("抽象问题为：", str_question)
        # 通过分类器获取问题模板编号
        question_template_num = self.classify_model.predict(str_question)-10
        # question_template_num = self.classify_model.predict(str_question)
        print("使用模板编号：", question_template_num)
        print(self.question_mode_dict)
        question_template = self.question_mode_dict[question_template_num]
        print("问题模板：", question_template)
        question_template_id_str = str(question_template_num)+"\t"+question_template
        return question_template_id_str

    # 根据问题模板的具体内容，构造cql语句，并查询
    def query_template(self,pos_question):
        # 调用问题模板类中的获取答案的方法
        try:
            # answer = self.questiontemplate.get_question_answer(self.pos_question, self.question_template_id_str)
            answer = self.questiontemplate.get_question_answer(pos_question, self.question_template_id_str)
        except:
            answer = "抱歉 暂时未查找到答案"
        return answer

    # 根据问题模板的具体内容，构造cql语句，并查询获取关系图
    def query_graph(self,pos_question):
        # 调用问题模板类中的获取答案的方法
        try:
            answer = self.questiongraph.get_question_answer(pos_question, self.question_template_id_str)
        except:
            answer = " "
        return answer

    def question_process2(self, question):
        # 接收问题
        self.raw_question = str(question).strip()
        # 对问题进行词性标注
        # self.pos_question = self.question_posseg()
        # print(self.pos_question)
        pos_temp, self.question_word, self.question_flag = self.question_posseg()
        # return self.pos_quesiton
        # 得到问题的模板
        # self.question_word, self.question_flag = self.question_posseg()
        self.question_template_id_str = self.get_question_template()
        print(self.question_template_id_str)

        pos_question, pos_word, pos_flag = self.question_posseg()
        # 查询图数据库,得到答案
        self.answer2 = self.query_graph(pos_question)
        return(self.answer2)

# q = Question()
# q.question_process('剧情类电影')
# temp = q.pos_question
# print(temp)
# for one in temp:
#     print(one)
# question_flag = ['剧情/tp', '类/q', '电影/n', '有/v', '哪些/r']
# # tag_index = question_flag.index("tp")
# # print(tag_index)
# jieba.re_han_default = re.compile("([\u4E00-\u9FD5a-zA-Z0-9+#&\._% ]+)", re.U)
# seg = pkuseg.pkuseg()
# text = "皮耶罗·德·贝纳迪 Piero De Bernardi写过哪些电影的剧本"
# result = seg.cut(text)
# print(result)