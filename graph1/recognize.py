from django.shortcuts import render
from graph1.static.plugins import jieba
import os

# 构建文件路径
file_path = os.path.join(os.path.dirname(__file__), 'dictionary.txt')


def cut_text(text):
    # 分词并返回结果
    jieba.load_userdict(file_path)
    return "/".join(jieba.cut(text))


def recognize_text(request):
    if request.method == 'POST':
        text = request.POST.get('user_text', '')  # 从HTML表单中获取文本内容

        # 实体识别
        entities = set()  # 使用set来保存唯一的实体
        entity_hyperlinks = {}  # 存储实体和对应的超链接
        # 读取 dictionary.txt 文件中的实体
        with open(file_path, 'r', encoding='utf-8') as f:
            entity_dict = set(f.read().splitlines())
        jieba.load_userdict(file_path)
        words = jieba.cut(text)
        for word in words:
            if word in entity_dict:
                entities.add(word)
                # 构建百度百科链接
                baidu_url = f'https://baike.baidu.com/item/{word}?fromModule=lemma_search-box'
                # 创建超链接
                hyperlink = f'<a href="{baidu_url}" target="_blank">{word}</a>'
                # 存储实体和对应的超链接
                entity_hyperlinks[word] = hyperlink

        # 标亮实体并添加超链接
        highlighted_text = text
        for entity, hyperlink in entity_hyperlinks.items():
            # 标亮实体
            highlighted_text = highlighted_text.replace(entity, f'<span class="highlight">{hyperlink}</span>')

        return render(request, 'recognize.html',
                      {'entities': entities, 'highlighted_text': highlighted_text, 'seg_word': cut_text(text)})
    else:
        return render(request, 'recognize.html')