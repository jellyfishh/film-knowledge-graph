#-*- coding: UTF-8 -*-

from py2neo import Graph, Node, Relationship, NodeMatcher

class Query():
    def __init__(self):
        # print("hello")
        # self.graph = Graph("http://localhost:7474/browser/", auth=('neo4j','000909'))
        self.graph = Graph("http://localhost:7474/browser/", auth=('neo4j','12345678'), name="neo4j")
        print("连接成功")

    # 问题类型0，查询电影得分
    def run(self, cql):
        # headers = {
        #     "Accept": "application/json; charset=UTF-8",
        # }
        # find_rela = self.graph.run(cql, headers=headers)
        result = []
        find_rela = self.graph.run(cql)
        for i in find_rela:
            result.append(i.items()[0][1])
        return result
