from django.urls import path
from graph1 import views, recognize, overview, relation, entity

urlpatterns = [
    path('sidebar/', views.sidebar),
    path('relation/', relation.search_relation),
    path('entity/', entity.search_entity),
    path('recognize/', recognize.recognize_text, name='recognize_text'),
    path('QA/', views.QA),
    path('ajax_handle_question/', views.ajax_handle_question),
    path('ajax_handle_question2/', views.ajax_handle_question2),
    path('overview/', views.show_overview)
]