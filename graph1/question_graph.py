from py2neo import Graph


class QuestionGraph():
    graph = Graph()

    def __init__(self):
        self.q_template_dict = {
            0: self.get_instance,
            1: self.get_nm_comment,
            2: self.get_nm_acted_in,
            3: self.get_nm_attribute_rating,
            4: self.get_nm_attribute_summary,
            5: self.get_act_actor_cooperation,
            6: self.get_act_dir_cooperation,
            7: self.get_act_auth_cooperation,
        }

    def connectDB(self):
        # 本人neo4j账户
        self.graph = Graph("http://localhost:7474", username="neo4j", password="12345678")
        # self.graph = Graph("http://localhost:7474", auth=("neo4j", "12345678"))

    # question是分词之后的问句{w.word}/{w.flag}，即（pos_quesiton），未替换成词性；template例如0\tnc 实例
    def get_question_answer(self, question, template):
        # 如果问题模板的格式不正确则结束
        assert len(str(template).strip().split("\t")) == 2
        template_id, template_str = int(str(template).strip().split("\t")[0]), str(template).strip().split("\t")[1]
        self.template_id = template_id
        self.template_str2list = str(template_str).split()

        # 预处理问题
        question_word, question_flag = [], []
        for one in question:
            word, flag = one.split("/")
            question_word.append(str(word).strip())
            question_flag.append(str(flag).strip())
        assert len(question_flag) == len(question_word)
        self.question_word = question_word
        self.question_flag = question_flag
        self.raw_question = question
        # 根据问题模板来做对应的处理，获取答案
        answer = self.q_template_dict[template_id]()
        return answer

    def get_act_name(self):
        ## 获取act在原问题中的下标
        tag_index = self.question_flag.index("act")
        ## 获取nac名称
        act_name = self.question_word[tag_index]
        return act_name

    def get_nm_name(self):
        ## 获取nm在原问题中的下标
        tag_index = self.question_flag.index("nm")
        ## 获取电影名称
        nm_name = self.question_word[tag_index]
        return nm_name

    # def get_nd_name(self):
    #     ## 获取nd在原问题中的下标
    #     tag_index = self.question_flag.index("nd")
    #     ## 获取nd名称
    #     nd_name = self.question_word[tag_index]
    #     return nd_name

    # def get_nau_name(self):
    #     ## 获取nau在原问题中的下标
    #     tag_index = self.question_flag.index("nau")
    #     ## 获取nau名称
    #     nau_name = self.question_word[tag_index]
    #     return nau_name

    def get_tp_name(self):
        ## 获取tp在原问题中的下标
        tag_index = self.question_flag.index("tp")
        ## 获取tp名称
        tp_name = self.question_word[tag_index]
        return tp_name

    # def get_num_x(self):
    #     x = re.sub(r'\D', "", "".join(self.question_word))
    #     return x

    # 0:nt 实例
    def get_instance(self):
        tp_name = self.get_tp_name()
        answer = self.graph.run("MATCH (n1:movie)- [r{relation:belong_to}] -> (n2 {name:\"" + tp_name
                                + "\"}) RETURN n1,r,n2").data()
        return answer

    # 1:nac acted_in关系
    # def get_nac_acted_in(self):
    #     nac_name = self.get_nac_name()
    #     cql = f"match (m:actor)-[k:ACTED_IN]->(n:movie) where m.name='{nac_name}' return n.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = nac_name + "出演过的电影的有" + str(answer) + "。"
    #     return final_answer
    #
    # 2:nac actor_cooperation
    def get_act_actor_cooperation(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        act_name = self.get_act_name()
        answer = self.graph.run("match(n:actor{name:\"" + act_name + "\"})-[r{relation:acted_in}]->(m:movie),"
                                                                     "(o:actor)-[r2:ACTED_IN]->(m) "
                                                                     "return n, r, m, r2, o").data()
        return answer

    #
    # 3:nac act_dir_cooperation
    def get_act_dir_cooperation(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        act_name = self.get_act_name()
        answer = self.graph.run("match(n1:actor{name:\"" + act_name +
                                "\"})-[r{relation:act_dir_cooperation}]->(n2:director) return n1, r, n2").data()
        return answer

    #
    # 4:nac act_auth_cooperation
    def get_act_auth_cooperation(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        act_name = self.get_act_name()
        answer = self.graph.run("match(n1:actor{name:\"" + act_name +
                                "\"})-[r{relation:act_auth_cooperation}]->(n2:author) return n1, r, n2").data()
        return answer

    # 5:nm short_comment
    def get_nm_comment(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        answer = self.graph.run(
            "match(n1:short_comments)-[r{relation:short_comment}]->(n2:movie{name:\"" + nm_name +
            "\"}) return n1, r, n2").data()
        return answer

    # 6:nd dir_auth_cooperation
    # def get_nd_dir_auth_cooperation(self):
    #     # 获取问题中的领域名词，这个是在原问题中抽取的
    #     nd_name = self.get_nd_name()
    #     cql = f"match(n:director)-[r:DIR_AUTH_COOPERATION]->(m:author) where n.name='{nd_name}' return m.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = "与" + nd_name + "合作过的编剧有" + str(answer) + "。"
    #     return final_answer
    #
    # # 7:nd directed
    # def get_nd_directed(self):
    #     # 获取问题中的领域名词，这个是在原问题中抽取的
    #     nd_name = self.get_nd_name()
    #     cql = f"match(n:director)-[r:DIRECTED]->(m:movie) where n.name='{nd_name}' return m.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = nd_name + "执导过的电影有" + str(answer) + "。"
    #     return final_answer
    #
    # # 8:nau write
    # def get_nau_write(self):
    #     # 获取问题中的领域名词，这个是在原问题中抽取的
    #     nau_name = self.get_nau_name()
    #     cql = f"match(n:author)-[r:WRITE]->(m:movie) where n.name='{nau_name}' return m.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = nau_name + "为这些电影写过剧本：" + str(answer) + "。"
    #     return final_answer

    # 9:nm 属性
    def get_nm_acted_in(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        answer = self.graph.run("match(n1:actor)-[r{relation:acted_in}]->(n2:movie{name:\"" + nm_name +
                                "\"}) return n1, r, n2").data()
        return answer

    # 10:nm rating
    def get_nm_attribute_rating(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        answer = self.graph.run("match(n:movie{name:\"" + nm_name + "\"}) return n").data()
        return answer

    # 11:nm summary
    def get_nm_attribute_summary(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        answer = self.graph.run("match(n:movie{name:\"" + nm_name + "\"}) return n").data()
        return answer
