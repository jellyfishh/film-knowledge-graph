# -*- coding: utf-8 -*-
from django.shortcuts import render
from graph1.Models.neo_model import Neo4j
import os
import json

neo_con = Neo4j()  # 预加载neo4j

relationCountDict = {}
filePath = os.path.abspath(os.path.join(os.getcwd(), "../graph1"))


def sortDict(relationDict):
    for i in range(len(relationDict)):
        relationName = relationDict[i]['rel']['relation']
        relationCount = relationCountDict.get(relationName)
        if relationCount is None:
            relationCount = 0
        relationDict[i]['relationCount'] = relationCount

    relationDict = sorted(relationDict, key=lambda item: item['relationCount'], reverse=True)

    return relationDict


def search_relation(request):
    ctx = {}
    if request.GET:
        db = neo_con
        entity1 = request.GET['entity1_text']
        relation = request.GET['relation_name_text']
        entity2 = request.GET['entity2_text']
        relation = relation.lower()
        searchResult = {}
        # 若只输入entity1,则输出与entity1有直接关系的实体和关系
        if len(entity1) != 0 and len(relation) == 0 and len(entity2) == 0:
            searchResult = db.findRelationByEntity(entity1)
            searchResult = sortDict(searchResult)
            if len(searchResult) > 0:
                return render(request, 'relation.html', {'searchResult': json.dumps(searchResult, ensure_ascii=False)})

        # 若只输入entity2则,则输出与entity2有直接关系的实体和关系
        if len(entity2) != 0 and len(relation) == 0 and len(entity1) == 0:
            searchResult = db.findRelationByEntity2(entity2)
            searchResult = sortDict(searchResult)
            if len(searchResult) > 0:
                return render(request, 'relation.html', {'searchResult': json.dumps(searchResult, ensure_ascii=False)})

        # 若输入entity1和relation，则输出与entity1具有relation关系的其他实体
        if len(entity1) != 0 and len(relation) != 0 and len(entity2) == 0:
            searchResult = db.findOtherEntities(entity1, relation)
            searchResult = sortDict(searchResult)
            if len(searchResult) > 0:
                return render(request, 'relation.html', {'searchResult': json.dumps(searchResult, ensure_ascii=False)})

        # 若输入entity2和relation，则输出与entity2具有relation关系的其他实体
        if len(entity2) != 0 and len(relation) != 0 and len(entity1) == 0:
            searchResult = db.findOtherEntities2(entity2, relation)
            searchResult = sortDict(searchResult)
            if len(searchResult) > 0:
                return render(request, 'relation.html', {'searchResult': json.dumps(searchResult, ensure_ascii=False)})

        # 若输入entity1和entity2，则输出两者之间具有的relation关系
        if len(entity1) != 0 and len(entity2) != 0 and len(relation) == 0:
            searchResult = db.findRelationBetween(entity1, entity2)
            searchResult = sortDict(searchResult)
            if len(searchResult) > 0:
                return render(request, 'relation.html', {'searchResult': json.dumps(searchResult, ensure_ascii=False)})

        # 若输入entity1和entity2并指定关系，则输出其关系图和列表
        if len(entity1) != 0 and len(entity2) != 0 and len(relation) != 0:
            searchResult = db.findRelationDetail(entity1, entity2, relation)
            searchResult = sortDict(searchResult)
            if len(searchResult) > 0:
                return render(request, 'relation.html', {'searchResult': json.dumps(searchResult, ensure_ascii=False)})

        ctx = {'title': '<h1>暂未找到相应的匹配</h1>'}
        return render(request, 'relation.html', {'ctx': ctx})

    return render(request, 'relation.html', {'ctx': ctx})
