from django import template

register = template.Library()


@register.filter(name='stringreplace')
def stringreplace(value, arg):
    return value.replace(arg, '')


@register.filter(name='addhighlight')
def addhighlight(value, arg):
    return f'<span class="highlight">{arg}</span>'
