#-*- coding: UTF-8 -*-

from graph1.query import Query
import re

class QuestionTemplate():
    def __init__(self):
        self.q_template_dict = {
            0: self.get_instance,
            1: self.get_nm_comment,
            2: self.get_nm_acted_in,
            3: self.get_nm_attribute_rating,
            4: self.get_nm_attribute_summary,
            5: self.get_act_actor_cooperation,
            6: self.get_act_dir_cooperation,
            7: self.get_act_auth_cooperation,
        }

        # 连接数据库
        self.graph = Query()
        # # # 测试数据库是否连接上
        # result = self.graph.run(f"match(n:actor)-[r:ACTED_IN]->(m:movie) where m.name='教父' return n.name")
        # print(result)
        # exit()

    # question是分词之后的问句{w.word}/{w.flag}，即（pos_quesiton），未替换成词性；template例如0\tnc 实例
    def get_question_answer(self, question, template):
        # 如果问题模板的格式不正确则结束
        assert len(str(template).strip().split("\t")) == 2
        template_id, template_str = int(str(template).strip().split("\t")[0]), str(template).strip().split("\t")[1]
        self.template_id = template_id
        self.template_str2list = str(template_str).split()

        # 预处理问题
        question_word, question_flag = [], []
        for one in question:
            word, flag = one.split("/")
            question_word.append(str(word).strip())
            question_flag.append(str(flag).strip())
        assert len(question_flag) == len(question_word)
        self.question_word = question_word
        self.question_flag = question_flag
        self.raw_question = question
        # 根据问题模板来做对应的处理，获取答案
        answer = self.q_template_dict[template_id]()
        return answer

    def get_act_name(self):
        ## 获取act在原问题中的下标
        tag_index = self.question_flag.index("act")
        ## 获取nac名称
        act_name = self.question_word[tag_index]
        return act_name

    def get_nm_name(self):
            ## 获取nm在原问题中的下标
            tag_index = self.question_flag.index("nm")
            ## 获取电影名称
            nm_name = self.question_word[tag_index]
            return nm_name

    # def get_nd_name(self):
    #     ## 获取nd在原问题中的下标
    #     tag_index = self.question_flag.index("nd")
    #     ## 获取nd名称
    #     nd_name = self.question_word[tag_index]
    #     return nd_name

    # def get_nau_name(self):
    #     ## 获取nau在原问题中的下标
    #     tag_index = self.question_flag.index("nau")
    #     ## 获取nau名称
    #     nau_name = self.question_word[tag_index]
    #     return nau_name

    def get_tp_name(self):
        ## 获取tp在原问题中的下标
        tag_index = self.question_flag.index("tp")
        ## 获取tp名称
        tp_name = self.question_word[tag_index]
        return tp_name

    # def get_num_x(self):
    #     x = re.sub(r'\D', "", "".join(self.question_word))
    #     return x

    # 0:nt 实例
    def get_instance(self):
        tp_name = self.get_tp_name()
        cql = f"match (m:movie)-[k:BELONG_TO]->(n:type) where n.name='{tp_name}' return m.name"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "、".join(answer_list)
        final_answer = tp_name + "类的电影有" + str(answer) + "。"
        return final_answer


    # 1:nac acted_in关系
    # def get_nac_acted_in(self):
    #     nac_name = self.get_nac_name()
    #     cql = f"match (m:actor)-[k:ACTED_IN]->(n:movie) where m.name='{nac_name}' return n.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = nac_name + "出演过的电影的有" + str(answer) + "。"
    #     return final_answer
    #
    # 2:nac actor_cooperation
    def get_act_actor_cooperation(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        act_name = self.get_act_name()
        cql = f"match(n:actor)-[r:ACTED_IN]->(m:movie),(o:actor)-[:ACTED_IN]->(m) where n.name='{act_name}' return o.name"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "、".join(answer_list)
        final_answer = act_name + "合作过的演员有" + str(answer) + "。"
        return final_answer
    #
    # 3:nac act_dir_cooperation
    def get_act_dir_cooperation(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        act_name = self.get_act_name()
        cql = f"match(n:actor)-[r:ACT_DIR_COOPERATION]->(m:director) where n.name='{act_name}' return m.name"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "、".join(answer_list)
        final_answer = act_name + "合作过的导演有" + str(answer) + "。"
        return final_answer
    #
    # 4:nac act_auth_cooperation
    def get_act_auth_cooperation(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        act_name = self.get_act_name()
        cql = f"match(n:actor)-[:ACT_AUTH_COOPERATION]->(m:author) where n.name='{act_name}' return m.name"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "、".join(answer_list)
        final_answer = act_name + "合作过的编剧有" + str(answer) + "。"
        return final_answer

    # 5:nm short_comment
    def get_nm_comment(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        cql = f"match (o:short_comments)-[l:SHORT_COMMENT]->(n:movie) where n.name='{nm_name}' return o.name"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "；".join(answer_list)
        final_answer = "与" + nm_name + "有关的评论有" + str(answer) + "。"
        return final_answer

    # 6:nd dir_auth_cooperation
    # def get_nd_dir_auth_cooperation(self):
    #     # 获取问题中的领域名词，这个是在原问题中抽取的
    #     nd_name = self.get_nd_name()
    #     cql = f"match(n:director)-[r:DIR_AUTH_COOPERATION]->(m:author) where n.name='{nd_name}' return m.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = "与" + nd_name + "合作过的编剧有" + str(answer) + "。"
    #     return final_answer
    #
    # # 7:nd directed
    # def get_nd_directed(self):
    #     # 获取问题中的领域名词，这个是在原问题中抽取的
    #     nd_name = self.get_nd_name()
    #     cql = f"match(n:director)-[r:DIRECTED]->(m:movie) where n.name='{nd_name}' return m.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = nd_name + "执导过的电影有" + str(answer) + "。"
    #     return final_answer
    #
    # # 8:nau write
    # def get_nau_write(self):
    #     # 获取问题中的领域名词，这个是在原问题中抽取的
    #     nau_name = self.get_nau_name()
    #     cql = f"match(n:author)-[r:WRITE]->(m:movie) where n.name='{nau_name}' return m.name"
    #     print(cql)
    #     answer = self.graph.run(cql)
    #     print(answer)
    #     answer_set = set(answer)
    #     answer_list = list(answer_set)
    #     answer = "、".join(answer_list)
    #     final_answer = nau_name + "为这些电影写过剧本：" + str(answer) + "。"
    #     return final_answer

    # 9:nm 属性
    def get_nm_acted_in(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        cql = f"match (o:actor)-[l:ACTED_IN]->(n:movie) where n.name='{nm_name}' return o.name"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "、".join(answer_list)
        final_answer = "出演" + nm_name + "的演员有" + str(answer) + "。"
        return final_answer

    # 10:nm rating
    def get_nm_attribute_rating(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        cql = f"match (m:movie) where m.name='{nm_name}' return m.rating"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "、".join(answer_list)
        final_answer = nm_name + "的评分为：" + str(answer) + "。"
        return final_answer

    # 11:nm summary
    def get_nm_attribute_summary(self):
        # 获取问题中的领域名词，这个是在原问题中抽取的
        nm_name = self.get_nm_name()
        cql = f"match (m:movie) where m.name='{nm_name}' return m.summary"
        print(cql)
        answer = self.graph.run(cql)
        print(answer)
        answer_set = set(answer)
        answer_list = list(answer_set)
        answer = "、".join(answer_list)
        final_answer = nm_name + "的简介为：" + str(answer) + "。"
        return final_answer

qt = QuestionTemplate()
# print(qt.get_question_answer(['霸王别姬/l', '有/v', '哪些/r', '相关/v', '评论/n'], '5	nm 评论'))