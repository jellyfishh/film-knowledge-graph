from py2neo import Graph, Node, Relationship, cypher, Path


class Neo4j():
    graph = Graph()

    def __init__(self):
        print("create neo4j class ...")

    def connectDB(self):
        # 本人neo4j账户
        self.graph = Graph("http://localhost:7474", username="neo4j", password="12345678")
        # self.graph = Graph("http://localhost:7474", auth=("neo4j", "12345678"))

    # 实体查询
    # 根据entity的名称返回所有对应的关系
    def getEntityRelationByEntity(self, value):
        answer = self.graph.run("MATCH (entity1) - [rel] - (entity2)  WHERE entity1.name = \"" + str(
            value) + "\" RETURN rel,entity2").data()
        return answer

    # 关系查询
    # 查找entity1及其对应的关系
    def findRelationByEntity(self, entity1):
        answer = self.graph.run("MATCH (n1 {name:\"" + str(entity1) + "\"})- [rel] -> (n2) RETURN n1,rel,n2").data()
        return answer

    # 查找entity2及其对应的关系
    def findRelationByEntity2(self, entity1):
        answer = self.graph.run("MATCH (n1)- [rel] -> (n2 {name:\"" + str(entity1) + "\"}) RETURN n1,rel,n2").data()
        return answer

    # 根据entity1和关系查找entity2
    def findOtherEntities(self, entity, relation):
        answer = self.graph.run("MATCH (n1 {name:\"" + str(entity) + "\"})- [rel {relation:\"" + str(relation)
                                + "\"}] -> (n2) RETURN n1,rel,n2").data()
        return answer

    # 根据entity2和关系查找entity1
    def findOtherEntities2(self, entity, relation):
        answer = self.graph.run("MATCH (n1)- [rel {relation:\"" + str(relation) + "\"}] -> (n2 {name:\"" + str(entity)
                                + "\"}) RETURN n1,rel,n2").data()
        return answer

    # 根据entity1和entity2查找关系
    def findRelationBetween(self, entity1, entity2):
        answer = self.graph.run("MATCH (n1 {name:\"" + str(entity1) + "\"})- [rel] -> (n2 {name:\"" + str(entity2)
                                + "\"}) RETURN n1,rel,n2").data()
        return answer

    # 根据entity1和entity2查找指定关系
    def findRelationDetail(self, entity1, entity2, relation):
        answer = self.graph.run("MATCH (n1 {name:\"" + str(entity1) + "\"})- [rel {relation:\"" + str(relation)
                                + "\"}] -> (n2 {name:\"" + str(entity2) + "\"}) RETURN n1,rel,n2").data()
        return answer
